import React, {useState} from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

import Seletor from './comp/Seletor';
import Bot from './comp/Bot';

export default function App() {
  const [time, setTime] = useState(new Date(0,0));
  return (
    <View style={styles.container}>
      <Text style={styles.head}>Timer 
      {time.getHours()}:{time.getMinutes()}:{time.getSeconds()}</Text>
      <View style={styles.timer}>
        <Seletor max={11} 
          onChange={(val)=>{setTime(new Date(time.setHours(val)))}}/>
        <Text style={styles.colon}>:</Text>
        <Seletor onChange={(val)=>{setTime(new Date(time.setMinutes(val)))}}/>
        <Text style={styles.colon}>:</Text>
        <Seletor onChange={(val)=>{setTime(new Date(time.setSeconds(val)))}}/>
      </View>
      <View style={styles.row}>
      <Bot label='Inicio/Pausa' />
      </View>
      <View style={styles.row}>
        <Bot label='Voltar' />
        <View style={{width:15}}></View>
        <Bot label='Apagar' />
      </View>

      <StatusBar style="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 26,
    backgroundColor: '#023',
    paddingHorizontal: 30,
    justifyContent:'space-evenly'
  },
  head:{
    color:'#fff',
    fontSize: 50,
    fontWeight:'bold',
    opacity:0.5,
    fontStyle: 'italic',
  },
  timer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  colon: {
    fontSize: 50,
    color: '#fff'
  },
  row:{
    flexDirection: 'row',
    gap:10,
  },
});
