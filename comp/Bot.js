import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import symbolicateStackTrace from 'react-native/Libraries/Core/Devtools/symbolicateStackTrace';

export default function Bot(props) {
  return (
    <TouchableOpacity style={styles.bot}>
      <Text style={styles.label}>{props.label}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  bot:{
    backgroundColor:'#fff9',
    borderRadius: 50,
    flex:1,
  },
  label:{
    fontSize:20,
    padding:10,
    textAlign:'center',
    color: '#023',
    fontWeight:'bold'
  },
});